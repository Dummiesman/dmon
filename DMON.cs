﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

/*
COPYRIGHT (C) 2015 DUMMIESMAN (AARO4130)
DO NOT REDISTRITUBE WITHOUT THIS HEADER
DO NOT MODIFY ANY PART OF THIS CODE AND CLAIM IT AS YOUR OWN
DO NOT CLAIM PARTS OR THIS ENTIRE PUBLICATION AS YOUR OWN
*/

class DMON
{
    //static memory
    public static string[] includePaths = new string[] { };

    //DMON
    Dictionary<string, object> vars = new Dictionary<string, object>();

    //public memory
    public bool DMONFeaturesetEnableIntParsing = true;
    public bool DMONFeaturesetEnableCharParsing = true;
    public bool DMONFeaturesetEnableArrayParsing = true;
    public bool DMONFeaturesetEnableInlineParsing = true;
    public bool DMONFeaturesetEnableIncludeParsing = true;
    public bool DMONFeaturesetEnableFloatParsing = true;
    public bool DMONFeaturesetEnableStringParsing = true;
    public bool DMONFeaturesetEnableBoolParsing = true;

    public bool DMONOptimizationSingleTypeArrays = false;
    public Type DMONOptimizationSingleTypeArraysType = null;

    static IEnumerable<string> ReadStringLines(string lines)
    {
        string line;
        using (var reader = new StringReader(lines))
        {
            while ((line = reader.ReadLine()) != null)
            {
                yield return line;
            }
        }
    }

    //used to parse values like "mystring"
    private bool lastParsedIsStackable = false;
    private object ParseDMONValue(string val)
    {
        lastParsedIsStackable = false;

        /* INCLUDE */
        if (DMONFeaturesetEnableIncludeParsing && val.StartsWith("include "))
        {
            int spaceIdx = val.IndexOf(" ");
            string includeFile = val.Substring(spaceIdx + 1, val.Length - spaceIdx - 1);

            //find in include paths
            foreach (string pth in includePaths)
            {
                if (File.Exists(pth + "\\" + includeFile))
                {
                    lastParsedIsStackable = true;

                    var retnTable =  new DMON().Parse(File.ReadAllText(pth + "\\" + includeFile));
                    retnTable.isIncludeOrInline = true;

                    return retnTable;
                }
            }

            //not found :(
            throw new Exception("Could not find include file " + includeFile + ". Are your include paths correct?");
        }

        /* INLINE DMON */
        if (DMONFeaturesetEnableInlineParsing && val[0] == '(')
        {
            List<string> vals = new List<string>();
            int lastCommaIDX = 1;
            for (int i = 1; i < val.Length - 1; i++)
            {
                char c = val[i];
                if (c == ',')
                {
                    vals.Add(val.Substring(lastCommaIDX, i - lastCommaIDX));
                    lastCommaIDX = i;
                }
            }

            //create DMON table, and parse values
            var tbl = new DMONTable<object>();
            tbl.isIncludeOrInline = true;

            foreach (string kvp in vals)
            {
                string[] kv = kvp.Trim().Split(':');
                string k = kv[0].TrimEnd();
                object v = ParseDMONValue(kv[1].TrimStart());

                tbl[k] = v;
            }

            lastParsedIsStackable = true;
            return tbl;
        }

        /* STRING */
        if (DMONFeaturesetEnableStringParsing && val[0] == '"')
        {
            //string variable
            int lastQuoteIndex = val.LastIndexOf("\"");
            if (lastQuoteIndex == -1)
            {
                throw new Exception("Cannot parse string, missing end quote.");
            }
            else {
                string parsedValue = val.Substring(1, lastQuoteIndex - 1);
                return parsedValue;
            }
        }

        /* CHAR */
        if (DMONFeaturesetEnableCharParsing && val[0] == '\'')
        {
            //char variable
            int firstQuoteIndex = val.IndexOf("'");
            int lastQuoteIndex = val.LastIndexOf("'");
            if (lastQuoteIndex == -1)
            {
                throw new Exception("Cannot parse char, missing end quote.");
            }
            else {
                firstQuoteIndex += 1;
                string parsedValue = val.Substring(firstQuoteIndex, lastQuoteIndex - firstQuoteIndex);
                return parsedValue[0];
            }
        }

        /* INT/ULONG/FLOAT */
        if ((char.IsNumber(val[0])) || (val[0] == '-' && char.IsNumber(val[1])))
        {
            //faster than indexof :P
            bool isDec = false;
            if (val.Length >= 3)
            {
                for (int i = 1; i < val.Length - 1; i++)
                {
                    if (val[i] == '.')
                        isDec = true;
                }
            }

            //integer, or decimal?
            if (DMONFeaturesetEnableFloatParsing && isDec)
            {
                //decimal
                return float.Parse(val);
            }
            else if (DMONFeaturesetEnableIntParsing)
            {
                //integer
                object intObj;
                intObj = ulong.Parse(val);

                if ((ulong)intObj <= int.MaxValue)
                    intObj = int.Parse(val);

                return intObj;
            }
        }

        /* ARRAY OF OBJECT */
        if (DMONFeaturesetEnableArrayParsing && val[0] == '[')
        {

            //FAST STRING SPLITTING. SPLIT() IS ~50% SLOWER
            List<string> vals = new List<string>();
            int lastCommaIDX = 1;
            for (int i = 1; i < val.Length - 1; i++)
            {
                char c = val[i];
                if (c == ',')
                {
                    vals.Add(val.Substring(lastCommaIDX, i - lastCommaIDX));
                    lastCommaIDX = i;
                }
            }

            //TRIM
            for (int i = 0; i < vals.Count; i++)
            {
                vals[i] = vals[i].Trim();
            }

            //parse each value in the array
            object[] aonArray = new object[vals.Count];
            for (int i = 0; i < vals.Count; i++)
            {
                object fnl = ParseDMONValue(vals[i]);
                aonArray[i] = fnl;
            }

            return aonArray;


        }

        /* BOOL */
        if (DMONFeaturesetEnableBoolParsing)
        {
            string lowerVal = val.ToLower();
            if (lowerVal == "true" || lowerVal == "false")
                return bool.Parse(val);
        }

        return null;
    }

    private string[] DMONFastParse(string line)
    {
        int foundSpaces = 0;
        int space3Index = -1;
        int space2Index = -1;
        int space1Index = -1;

        for (int i = 0; i < line.Length; i++)
        {
            char tvc = line[i];
            if (tvc == ' ')
            {
                if (foundSpaces == 0)
                {
                    space1Index = i;
                }
                else if (foundSpaces == 1)
                {
                    space2Index = i;
                }
                else if (foundSpaces == 2)
                {
                    space3Index = i;
                }
                foundSpaces++;

                if (foundSpaces == 3)
                    break;
            }
        }


        if (foundSpaces == 0)
        {
            return new string[] { line };
        }
        else if (foundSpaces == 1)
        {
            string sideA = line.Substring(0, space1Index);
            string sideB = line.Substring(space1Index + 1, line.Length - space1Index - 1);
            return new string[] { sideA, sideB };
        }
        else if (foundSpaces >= 2)
        {
            string sideA = line.Substring(0, space1Index);
            string sideB = line.Substring(space1Index + 1, space2Index - space1Index - 1);
            string sideC = line.Substring(space2Index + 1, line.Length - space2Index - 1);
            return new string[] { sideA, sideB, sideC };
        }

        return null;
    }

    
    public DMONTable<object> Parse(string parseMe)
    {
        DMONTable<object> parentTable = new DMONTable<object>();
        List<DMONTable<object>> tableStack = new List<DMONTable<object>>();
        int nullKeyAmount = 0;

        /* PARSE DMON LINES */
        Stopwatch st = new Stopwatch();
        int linesP = -1;
        foreach (string l in ReadStringLines(parseMe))
        {
            /*if(linesP >= 0)
            {
                Console.WriteLine("Line" + linesP + " took " + st.ElapsedMilliseconds + "ms");
            }
            linesP++;
            st.Restart();*/

            string trimmedVal = l.TrimStart();


            //Skip empty lines/comment lines
            if (trimmedVal.Length < 2) //empty
                continue;
            if (trimmedVal[0] == '/' && trimmedVal[1] == '/')
                continue;


            string[] sl = DMONFastParse(trimmedVal);

            /* Group Close Character */
            if (sl[0] == "}")
            {
                //CLOSE STACK
                if (tableStack.Count > 0)
                {
                    if (tableStack.Count > 1)
                    {
                        //combine stack
                        tableStack[tableStack.Count - 2][tableStack[tableStack.Count - 1].name] = tableStack[tableStack.Count - 1];
                        //pop!
                        tableStack.RemoveAt(tableStack.Count - 1);
                    }
                    else {
                        //last stack element!! :D
                        parentTable[tableStack[0].name] = tableStack[0];
                        //pop it!
                        tableStack.RemoveAt(0);
                    }
                    

                }
                else {
                    throw new Exception("TableStackException : Missing table to encapsulate previous!");
                }
                continue;
            }

            //Everything past this point requires 2 elements
            //quit early if that's not the case
            if (sl.Length < 2)
                continue;

            /* Group Opener*/
            if (sl[1] == "{")
            {
                //new luatable
                string nm = sl[0];
                if (nm == "?")
                {
                    nm = "nullkey_" + nullKeyAmount;
                    nullKeyAmount++;
                }
                DMONTable<object> tbl = new DMONTable<object>() { name = nm };
                //push!
                int tblid = tableStack.Count;
                tableStack.Add(tbl);

                //no need to do anything else
                continue;
            }

            /* Value */
            if (sl[1] == "=")
            {
                //assign :D
                bool isVar = (sl[0].StartsWith("*"));
                object finalAssignment = ParseDMONValue(sl[2]);
                if (!isVar)
                {
                    if (tableStack.Count > 0)
                    {
                        tableStack[tableStack.Count - 1][sl[0]] = finalAssignment;
                    }
                    else {
                        //UnityEngine.Debug.Log ("parentTable adding " + sl[0]);
                        parentTable[sl[0]] = finalAssignment;
                    }
                }
                else
                {
                    //var
                    vars[sl[0].Remove(0, 1)] = finalAssignment;
                }

            }

        }

        return parentTable;

    }
}

