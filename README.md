# DMON #

### What is this repository for? ###

* DMON, an object notation format that is more lightweight than JSON
* Current version is the initial release version (see commits)

### How do I get set up? ###

* Clone or download source code and add it to your C# 3.5 or higher project
* Use wiki examples to get yourself started 
