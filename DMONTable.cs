﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

/*
This code is a modification of 
http://stackoverflow.com/a/31568187/2377834
Original code is by Jashaszun
*/

public class DMONTable<T> where T : class
{
		
	public string name = "";
	public  bool isValue;
    public bool isIncludeOrInline;
	private T value = null;
	private Dictionary<object, DMONTable<T>> properties = new Dictionary<object, DMONTable<T>> ();

    public void ToClass(object C)
    {
        Type cType = C.GetType();
        PropertyInfo[] pinfo = cType.GetProperties();
        FieldInfo[] finfo = cType.GetFields();
        foreach (PropertyInfo p in pinfo)
        {
            if (properties.ContainsKey(p.Name))
            {
                p.SetValue(C, Convert.ChangeType(properties[p.Name].value, p.PropertyType));
            }
        }
        foreach (FieldInfo f in finfo)
        {
            if (properties.ContainsKey(f.Name))
            {
                
                f.SetValue(C, Convert.ChangeType(properties[f.Name].value, f.FieldType));
            }
        }
    }

    public void FromClass(object C)
    {
        Type cType = C.GetType();
        PropertyInfo[] pinfo = cType.GetProperties();
        FieldInfo[] finfo = cType.GetFields();
        foreach (PropertyInfo p in pinfo)
        {
            properties[p.Name] = new DMONTable<T>() { isValue = true, value = (T)p.GetValue(C) };
        }
        foreach (FieldInfo f in finfo)
        {
            properties[f.Name] = new DMONTable<T>() { isValue = true, value = (T)f.GetValue(C) };
        }
    }

    public static implicit operator DMONTable<T> (T val)
	{
		if (val is DMONTable<T>)
			return (DMONTable<T>)val;
		return new DMONTable<T> () { isValue = true, value = val };
	}

	public static implicit operator T (DMONTable<T> table)
	{
		if (table.isValue)
			return table.value;
		return table;
	}
		
	public int Count {
		get {
			return properties.Count;
		}
	}
		
    public T ExtractValue()
    {
        if (isValue)
            return value;
        return null;
    }

    

	public DMONTable<T> this [object property] {
		get {

            if (isValue)
            {
                return null;
            }
				
			if (properties.ContainsKey (property))
				return properties [property];

            if (property is int)
            {
                int id = (int)property;
                if(properties.Count > id)
                {
                    return properties.ElementAt(id).Value;
                }
                else
                {
                    throw new Exception("Index out of range");
                }
            }
            DMONTable<T> table = new DMONTable<T> ();
			properties.Add (property, table);
			return table;
		}
		set {
			if (!properties.ContainsKey (property))
				properties.Add (property, value);
			else
				properties [property] = value;
		}
	}
}
